$(function(){
$("#elastic_grid_demo").elastic_grid({	
	'hoverDirection': true,
	'hoverDelay': 0,
	'hoverInverse': false,
	'expandingSpeed': 500,
	'expandingHeight': 500,
	'items' :
		[
			{
			'title' : 'Carpenters 1',
			'description'   : 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters.',
			'thumbnail' : ['img/portfolio/small/small/1.jpg'],
			'large' : ['img/portfolio/large/large/1.jpg'],
			'button_list'   :
			[
			],
			'tags'  : ['All']
			},

			{
			'title' : 'Carpenters 2',
			'description'   : 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters.',
			'thumbnail' : ['img/portfolio/small/small/2.jpg'],
			'large' : ['img/portfolio/large/large/2.jpg'],
			'button_list'   :
			[
			],
			'tags'  : ['Furniture','All']
			},

			{
			'title' : 'Carpenters 3',
			'description'   : 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters.',
			'thumbnail' : ['img/portfolio/small/small/3.jpg'],
			'large' : ['img/portfolio/large/large/3.jpg'],
			'button_list'   :
			[
			],
			'tags'  : ['Design', 'All']
			},

			{
			'title' : 'Carpenters 4',
			'description'   : 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters.',
			'thumbnail' : ['img/portfolio/small/small/4.jpg'],
			'large' : ['img/portfolio/large/large/4.jpg'],
			'button_list'   :
			[
			],
			'tags'  : ['Furniture','All']
			},

			{
			'title' : 'Carpenters 5',
			'description'   : 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters.',
			'thumbnail' : ['img/portfolio/small/small/5.jpg'],
			'large' : ['img/portfolio/large/large/5.jpg'],
			'button_list'   :
			[
			],
			'tags'  : ['Design','All']
			},

			{
			'title' : 'Carpenters 6',
			'description'   : 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters.',
			'thumbnail' : ['img/portfolio/small/small/6.jpg'],
			'large' : ['img/portfolio/large/large/6.jpg'],
			'button_list'   :
			[
			],
			'tags'  : ['Furniture','All']
			},

			{
			'title' : 'Carpenters 7',
			'description'   : 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters.',
			'thumbnail' : ['img/portfolio/small/small/7.jpg'],
			'large' : ['img/portfolio/large/large/7.jpg'],
			'button_list'   :
			[
			],
			'tags'  : ['Design','All']
			}			
		]
	});
});